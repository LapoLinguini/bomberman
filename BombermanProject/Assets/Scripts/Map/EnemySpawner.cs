using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    MapBuilding mapB;
    private void Awake()
    {
        mapB = FindObjectOfType<MapBuilding>();
    }
    void Update()
    {
        transform.position = mapB.newEnemyPos;
    }
}
