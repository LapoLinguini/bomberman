using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEditor;
using UnityEngine;

public class MapBuilding : MonoBehaviour
{
    [Header("Tiles:")]
    [SerializeField] GameObject Wall;
    [SerializeField] GameObject Block;
    [SerializeField] GameObject Box;
    [SerializeField] GameObject whiteTiles;
    [SerializeField] GameObject blackTiles;
    [Header("Players:")]
    [SerializeField] GameObject Player1;
    [SerializeField] GameObject Player2;
    [Header("Enemies:")]
    [SerializeField] GameObject Enemies;
    [SerializeField] GameObject EnemySpawner;
    [SerializeField] GameObject spawningEnemies;

    Vector2[,] myGrid;
    int xValue;
    int yValue;
    bool mapBuilt;

    int xBlockSpawn = -7;
    int numberOfInstBlocks = 0;
    int numberOfEnemies = 0;
    bool spawnEnemy;

    bool alternateInst = false;
    int randomInt;
    bool randomBoxes;

    int randomEnemyInt;
    bool randomEnemySpawn;

    Vector2 newPos;

    [HideInInspector] public Vector2 newEnemyPos;
    [HideInInspector] public bool finishedSpawning;
     public bool startedLevel;

    public LayerMask nonWalkable;
    public LayerMask enemyLayer;

    GameManager gm;
    private void Awake()
    {
        gm = FindObjectOfType<GameManager>();
    }
    private void Start()
    {
        finishedSpawning = false;
        startedLevel= false;

        xValue = 19;
        yValue = 11;

        mapBuilt = false;

        numberOfEnemies = 0;

        InizializeMatrix();
    }
    private void InizializeMatrix()
    {
        myGrid = new Vector2[xValue, yValue];

        EnemySpawner.SetActive(false);
        spawningEnemies.SetActive(false);

        for (int i = 0; i < xValue; i++)
        {
            for (int j = 0; j < yValue; j++)
            {
                //spawna la matrice partendo dal transform del gameObject
                myGrid[i, j] = new Vector2(i + transform.position.x, j + transform.position.y);
            }
        }
        //riempie la matrice
        StartCoroutine(FillMatrix());

        if (!GameManager.multiplayer)
        {
            StartCoroutine(SpawningEnemies());
        }
    }
    IEnumerator FillMatrix()
    {
        for (int i = 0; i < xValue; i++)
        {
            for (int j = 0; j < yValue; j++)
            {
                newPos = myGrid[i, j];

                //spawna le barriere dell'arena ai limiti della matrice
                if (newPos.x == -9 || newPos.x == 9 || newPos.y == -5 || newPos.y == 5)
                {
                    var Walls = Instantiate(Wall, newPos, Quaternion.identity);
                    Walls.transform.parent = gameObject.transform;
                }
                //spawna i blocchi ad una y prefissata e ad una x che varia ogni 4 blocchi spawnati
                else if (newPos == new Vector2(xBlockSpawn, 3) || newPos == new Vector2(xBlockSpawn, 1) || newPos == new Vector2(xBlockSpawn, -1) || newPos == new Vector2(xBlockSpawn, -3))
                {
                    var Blocks = Instantiate(Block, newPos, Quaternion.identity);
                    Blocks.transform.parent = gameObject.transform;
                    numberOfInstBlocks++;
                    if (numberOfInstBlocks == 4)
                    {
                        xBlockSpawn += 2;
                        numberOfInstBlocks = 0;
                    }
                }
                // -- MULTIPLAYER GENERATION -- (spawn delle casse raddoppiato per rendere pi� competitiva la partita)
                else if ((newPos != new Vector2(-8, 4) && newPos != new Vector2(-7, 4) && newPos != new Vector2(-8, 3) && newPos != new Vector2(8, -4) && newPos != new Vector2(7, -4) && newPos != new Vector2(8, -3) && newPos != new Vector2(-8, 2) && newPos != new Vector2(-6, 4) && newPos != new Vector2(8, -2) && newPos != new Vector2(6, -4)) && GameManager.multiplayer)
                {
                    randomInt = Random.Range(0, 4);

                    if (randomInt == 0)
                        randomBoxes = false;
                    else
                        randomBoxes = true;
                    if (randomBoxes)
                    {
                        var Boxes = Instantiate(Box, newPos, Quaternion.identity);
                        Boxes.transform.parent = gameObject.transform;
                    }
                }
                //Player1 e Player2 startano sempre con lo stesso "spawn" (3 tasselli liberi + 2 casse che bloccano il path)
                else if (newPos == new Vector2(-8, 2) || newPos == new Vector2(-6, 4) || newPos == new Vector2(8, -2) || newPos == new Vector2(6, -4) && GameManager.multiplayer)
                {
                    var Boxes = Instantiate(Box, newPos, Quaternion.identity);
                    Boxes.transform.parent = gameObject.transform;
                }
                // -- SINGLEPLAYER GENERATION -- (spawn delle casse basso per lasciare spazio ai nemici)
                else if ((newPos != new Vector2(-8, 4) && newPos != new Vector2(-7, 4) && newPos != new Vector2(-8, 3) && newPos != new Vector2(-8, 2) && newPos != new Vector2(-6, 4)) && !GameManager.multiplayer)
                {
                    randomInt = Random.Range(0, 5);

                    if (randomInt == 0 || randomInt == 1)
                        randomBoxes = true;
                    else
                        randomBoxes = false;
                    if (randomBoxes)
                    {
                        var Boxes = Instantiate(Box, newPos, Quaternion.identity);
                        Boxes.transform.parent = gameObject.transform;
                    }
                }
                //il player starta sempre con lo stesso "spawn" (3 tasselli liberi + 2 casse che bloccano il path)
                else if (newPos == new Vector2(-8, 2) || newPos == new Vector2(-6, 4) && !GameManager.multiplayer)
                {
                    var Boxes = Instantiate(Box, newPos, Quaternion.identity);
                    Boxes.transform.parent = gameObject.transform;
                }
                else if (newPos == new Vector2(-8, 4) && !GameManager.multiplayer)
                {
                    Instantiate(Player1, newPos, Quaternion.identity);
                }
                //alterna lo spawn di tasselli bianchi e neri (pura estetica bruh)
                if (!alternateInst)
                {
                    var wTiles = Instantiate(whiteTiles, newPos, Quaternion.identity);
                    wTiles.transform.parent = gameObject.transform;
                    alternateInst = true;
                }
                else if (alternateInst)
                {
                    var bTiles = Instantiate(blackTiles, newPos, Quaternion.identity);
                    bTiles.transform.parent = gameObject.transform;
                    alternateInst = false;
                }
                //spawna Player / Player1 & Player2 a seconda della modalit� scelta 
                if (newPos == new Vector2(9, 5) && GameManager.multiplayer)
                {
                    Instantiate(Player1, new Vector2(-8, 4), Quaternion.identity);
                    Instantiate(Player2, new Vector2(8, -4), Quaternion.identity);
                    gm.CanMove = true;
                    startedLevel = true;
                }
                if (newPos == new Vector2(9, 5))
                {
                    mapBuilt = true;
                }

                yield return new WaitForSeconds(0.01f);
            }
        }
        yield return null;
    }
    IEnumerator SpawningEnemies()
    {

        for (int i = 0; i < xValue; i++)
        {
            for (int j = 0; j < yValue; j++)
            {
                EnemySpawner.SetActive(true);
                spawningEnemies.SetActive(true);

                newEnemyPos = myGrid[i, j];

                randomEnemyInt = Random.Range(0, 14);

                if (randomEnemyInt == 0)
                    randomEnemySpawn = true;
                else
                    randomEnemySpawn = false;
                if (randomEnemySpawn)

                spawnEnemy = true;

                if (Physics2D.OverlapCircle(newEnemyPos, 0.5f, nonWalkable))
                {
                    //Debug.Log("DONT SPAWN");
                    spawnEnemy = false;
                }
                if ((Physics2D.OverlapCircle(newEnemyPos + Vector2.up, 0.5f, nonWalkable) && Physics2D.OverlapCircle(newEnemyPos + Vector2.down, 0.5f, nonWalkable) && Physics2D.OverlapCircle(newEnemyPos + Vector2.left, 0.5f, nonWalkable) && Physics2D.OverlapCircle(newEnemyPos + Vector2.right, 0.5f, nonWalkable)))
                {
                    //Debug.Log("BLOCKED ALL DIRECTIONS");
                    spawnEnemy = false;
                }
                if (Physics2D.OverlapCircle(newEnemyPos, 0.5f, enemyLayer))
                {
                    //Debug.Log("DONT SPAWN");
                    spawnEnemy = false;
                }
                if (newEnemyPos == new Vector2(9, 5) && numberOfEnemies < gm.MaxNumberOfEnemies)
                {
                    i = 0;
                    j = 0;
                }
                if (numberOfEnemies == gm.MaxNumberOfEnemies && mapBuilt)
                {
                    Debug.Log("startedLevel");
                    EnemySpawner.SetActive(false);
                    spawningEnemies.SetActive(false);
                    gm.CanMove = true;
                    finishedSpawning= true;
                    startedLevel = true;
                }
                if (numberOfEnemies < gm.MaxNumberOfEnemies && spawnEnemy && randomEnemySpawn && (newEnemyPos != new Vector2(-8, 4) && newEnemyPos != new Vector2(-7, 4) && newEnemyPos != new Vector2(-8, 3)))
                {
                    Instantiate(Enemies, newEnemyPos, Quaternion.identity);
                    numberOfEnemies++;
                    //Debug.Log("SPAWN");
                }
                yield return new WaitForSeconds(0.025f);
            }
        }
        yield return null;
    }
}
