using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

public class AudioManager : MonoBehaviour
{
    public AudioMixer audioMixer;
    public Slider musicSlider;
    public Slider soundSlider;
    [HideInInspector] public float musicVolume;
    [HideInInspector] public float soundVolume;

    private void Start()
    {
        musicSlider.value = PlayerPrefs.GetFloat("save1", musicVolume);
        soundSlider.value = PlayerPrefs.GetFloat("save2", soundVolume);
    }
    public void MusicVolume(float volume)
    {
        audioMixer.SetFloat("MusicVolume", volume);
        musicVolume= volume;
        PlayerPrefs.SetFloat("save1", musicVolume);
    }
    public void SoundVolume(float volume)
    {
        audioMixer.SetFloat("SoundVolume", volume);
        soundVolume = volume;
        PlayerPrefs.SetFloat("save2", soundVolume);
    }
}
