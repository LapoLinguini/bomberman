using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    [Header("Player1 Stats: ")]
    [SerializeField] public float speed1 = 2f;
    [SerializeField] public float TotalBombs1;
    [SerializeField] public int explosionRadius1;

    [Header("Player2 Stats: ")]
    [SerializeField] public float speed2 = 2f;
    [SerializeField] public float TotalBombs2;
    [SerializeField] public int explosionRadius2;

    [Header("WIN/LOSE State: ")]
    [SerializeField] public bool Player1Wins;
    [SerializeField] public bool Player2Wins;
    [SerializeField] public bool Draw;
    [SerializeField] public bool Lose;
    [SerializeField] public bool Win;

    [Header("START GAME: ")]
    public bool CanMove;

    [Header("N� of Enemies: ")]
    [SerializeField] public int MaxNumberOfEnemies;
    [SerializeField] public int NumberOfEnemies;

    [HideInInspector] public int[] spawnRadius;

    public static bool multiplayer;

    //audio things
    [HideInInspector] public float speedCheck1;
    [HideInInspector] public float speedCheck2;
    [HideInInspector] public int radCheck1;
    [HideInInspector] public int radCheck2;
    [HideInInspector] public bool bombCheck1;
    [HideInInspector] public bool bombCheck2;
    [HideInInspector] public bool PlayerDed1;
    [HideInInspector] public bool PlayerDed2;

    [Header("Sound: ")]
    [HideInInspector] public bool enemyDed;
    [HideInInspector] public bool boomBomb;

    public AudioSource SoundSource;
    public AudioSource MusicSource1;
    public AudioSource MusicSource2;
    [SerializeField] AudioClip enemyDead;
    [SerializeField] AudioClip playerDed;
    [SerializeField] AudioClip spawningClip;
    [SerializeField] AudioClip Level1Theme;
    [SerializeField] AudioClip bombExpl;

    MapBuilding mb;

    bool closeIf;

    private void Awake()
    {
        closeIf= false;
        mb = FindObjectOfType<MapBuilding>();
    }
    private void Start()
    {
        TotalBombs1 = 1f;
        TotalBombs2 = 1f;
        speed1 = 2.5f;
        speed2 = 2.5f;
        explosionRadius1 = 1;
        explosionRadius2 = 1;

        speedCheck1 = speed1;
        speedCheck2 = speed2;
        radCheck1 = explosionRadius1;
        radCheck2 = explosionRadius2;
        bombCheck1 = false;
        bombCheck2 = false;

        CanMove = false;
        NumberOfEnemies = MaxNumberOfEnemies;
    }
    private void Update()
    {
        if(enemyDed)
        {
            SoundSource.PlayOneShot(enemyDead, 0.5f);
            enemyDed = false;
        }
        if (PlayerDed1)
        {
            SoundSource.PlayOneShot(playerDed, 0.3f);
            PlayerDed1= false;
        }
        if (PlayerDed2)
        {
            SoundSource.PlayOneShot(playerDed, 0.3f);
            PlayerDed2= false;
        }
        if (!mb.finishedSpawning)
        {
            MusicSource1.Play();
            mb.finishedSpawning = true;
        }
        if (mb.startedLevel && !closeIf)
        {
            MusicSource1.Stop();
            MusicSource2.Play();
            mb.startedLevel = false;
            closeIf = true;
        }
        if(boomBomb)
        {
            SoundSource.PlayOneShot(bombExpl, 0.2f);
            boomBomb = false;
        }


        Debug.Log(mb.startedLevel);

        if (Input.GetKeyDown(KeyCode.F11))
        {
            Screen.fullScreen = !Screen.fullScreen;
        }
    }
}
