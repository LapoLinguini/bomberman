using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEditor;
using UnityEngine;

public class EMovement : MonoBehaviour
{
    [SerializeField] LayerMask nonWalkLayer;
    [SerializeField] float speed;

    public bool upMove;
    public bool downMove;
    public bool rightMove;
    public bool leftMove;

    bool stopRandomizer;
    bool newPosCalculate;
    bool pathfinding;

    bool Ded;

    float randomizer;

    Vector2 oldPos, newPos;

    GameManager gm;
    private void Start()
    {
        Ded = false;
        gm = FindObjectOfType<GameManager>();
        newPosCalculate = true;
    }

    private void Update()
    {
        oldPos = new Vector2(transform.position.x, transform.position.y);

        if (gm.CanMove)
        {
            CheckForWall();
            Randomizer();
            DirectionAndMovement();
        }
        if(Ded)
        {
            gm.NumberOfEnemies--;
            gameObject.SetActive(false);
        }
        if (gm.Lose)
        {
            gameObject.GetComponent<Collider2D>().enabled = false;
        }
    }
    private void CheckForWall()
    {
        if (!upMove && !downMove && !leftMove && !rightMove)
        {
            if (Physics2D.OverlapCircle(oldPos + Vector2.up, 0.5f, nonWalkLayer))
            {
                upMove = false;
            }
            else
            {
                upMove = true;
            }
            if (Physics2D.OverlapCircle(oldPos + Vector2.down, 0.5f, nonWalkLayer))
            {
                downMove = false;
            }
            else
            {
                downMove = true;
            }
            if (Physics2D.OverlapCircle(oldPos + Vector2.left, 0.5f, nonWalkLayer))
            {
                leftMove = false;
            }
            else
            {
                leftMove = true;
            }
            if (Physics2D.OverlapCircle(oldPos + Vector2.right, 0.5f, nonWalkLayer))
            {
                rightMove = false;
            }
            else
            {
                rightMove = true;
            }
        }
    }
    private void Randomizer()
    {
        if (!stopRandomizer)
        {
            randomizer = Random.Range(0, 4);
            newPosCalculate = true;
        }
    }
    private void DirectionAndMovement()
    {
        if (upMove && randomizer == 0)
        {
            stopRandomizer = true;
            if (newPosCalculate)
            {
                newPos = (oldPos + Vector2.up);
                newPosCalculate = false;
            }
            transform.position = Vector3.MoveTowards(transform.position, newPos, speed * Time.deltaTime);
            pathfinding = false;
            downMove = false;
            leftMove = false;
            rightMove = false;
        }
        if (downMove && randomizer == 1)
        {
            stopRandomizer = true;
            if (newPosCalculate)
            {
                newPos = (oldPos + Vector2.down);
                newPosCalculate = false;
            }
            transform.position = Vector3.MoveTowards(transform.position, newPos, speed * Time.deltaTime);
            pathfinding = false;
            upMove = false;
            leftMove = false;
            rightMove = false;
        }
        if (leftMove && randomizer == 2)
        {
            stopRandomizer = true;
            if (newPosCalculate)
            {
                newPos = (oldPos + Vector2.left);
                newPosCalculate = false;
            }
            transform.position = Vector3.MoveTowards(transform.position, newPos, speed * Time.deltaTime);
            pathfinding = false;
            downMove = false;
            upMove = false;
            rightMove = false;
        }
        if (rightMove && randomizer == 3)
        {
            stopRandomizer = true;
            if (newPosCalculate)
            {
                newPos = (oldPos + Vector2.right);
                newPosCalculate = false;
            }
            transform.position = Vector3.MoveTowards(transform.position, newPos, speed * Time.deltaTime);
            pathfinding = false;
            downMove = false;
            leftMove = false;
            upMove = false;
        }
        if (new Vector2(transform.position.x, transform.position.y) == newPos && !pathfinding)
        {
            upMove = false;
            downMove = false;
            rightMove = false;
            leftMove = false;
            stopRandomizer = false;
            pathfinding = true;
        }
    }
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Explosion")
        {
            Ded = true;
            gm.enemyDed = true;
        }
    }
    //    private void OnDrawGizmos()
    //{
    //    Handles.color = Color.red;
    //    Handles.DrawWireDisc(oldPos + Vector2.up, Vector3.back, 0.5f);
    //    Handles.DrawWireDisc(oldPos + Vector2.down, Vector3.back, 0.5f);
    //    Handles.DrawWireDisc(oldPos + Vector2.left, Vector3.back, 0.5f);
    //    Handles.DrawWireDisc(oldPos + Vector2.right, Vector3.back, 0.5f);
    //}
}
