using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour
{
    public AudioSource MusicSource;
    public AudioSource SoundSource;
    public AudioClip buttonClick;
    [SerializeField] GameObject mainMenu;
    [SerializeField] GameObject optionsMenu;
    [SerializeField] GameObject modeSelect;

    private void Start()
    {
        mainMenu.SetActive(true);
        Time.timeScale = 1f;
    }
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.F11))
        {
            Screen.fullScreen = !Screen.fullScreen;
        }
    }
    public void OptionsMenu()
    {
        SoundSource.PlayOneShot(buttonClick);
        mainMenu.SetActive(false);
        optionsMenu.SetActive(true);
    }
    public void StartGame()
    {
        SoundSource.PlayOneShot(buttonClick);
        mainMenu.SetActive(false);
        modeSelect.SetActive(true);
    }
    public void BackButton()
    {
        SoundSource.PlayOneShot(buttonClick);
        optionsMenu.SetActive(false);
        modeSelect.SetActive(false);
        mainMenu.SetActive(true);
    }
    public void MultiplayerMode()
    {
        SoundSource.PlayOneShot(buttonClick);
        SceneManager.LoadScene("UomoBomba");
        GameManager.multiplayer = true;
    }
    public void SingleplayerMode()
    {
        SoundSource.PlayOneShot(buttonClick);
        SceneManager.LoadScene("UomoBomba");
        GameManager.multiplayer = false;
    }
    public void QuitGame()
    {
        SoundSource.PlayOneShot(buttonClick);
        Application.Quit();
    }

}
