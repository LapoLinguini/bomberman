using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenuManager : MonoBehaviour
{
    [SerializeField] GameObject PauseMenu;
    [SerializeField] GameObject OptionsMenu;
    [SerializeField] GameObject YouLostMenu;
    [SerializeField] GameObject YouWonMenu;
    [SerializeField] GameObject DrawMenu;
    [SerializeField] GameObject Player1WinMenu;
    [SerializeField] GameObject Player2WinMenu;
    [SerializeField] GameObject spawningEnemies;

    public AudioSource MusicSource;
    public AudioSource MusicSource2;
    public AudioSource SoundSource;
    public AudioClip buttonClick;
    public AudioClip winSound;
    public AudioClip loseSound;

    bool paused;
    bool gameFinished;
    bool inOptions;

    GameManager gm;

    
    private void Awake()
    {
        gm = FindObjectOfType<GameManager>();
    }
    private void Start()
    {
        inOptions = false;
        paused = false;
        gameFinished = false;
    }
    private void Update()
    {
        if (!gameFinished && !inOptions)
        {
            PauseGame();
        }

        if(!gameFinished)
            GameResults();
     
        if (gm.Player1Wins && gm.Player2Wins)
        {
            gameFinished = false;
            SoundSource.Stop();
            MusicSource2.Stop();
            gm.Draw = true;
            gm.Player1Wins = false;
            gm.Player2Wins = false;
            Player1WinMenu.SetActive(false);
            Player2WinMenu.SetActive(false);
        }
        if (gm.Win && gm.Lose)
        {
            SoundSource.Stop();
            MusicSource2.Stop();
            gm.NumberOfEnemies = gm.MaxNumberOfEnemies;
            gameFinished = false;
            gm.Draw = true;
            gm.Win = false;
            gm.Lose = false;
            YouLostMenu.SetActive(false);
            YouWonMenu.SetActive(false);
        }
        if (gm.NumberOfEnemies == 0)
        {
            gm.Win = true;
        }
    }
    private void PauseGame()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
            paused = !paused;

        if (paused)
        {
            spawningEnemies.SetActive(false);
            PauseMenu.SetActive(true);
            Time.timeScale = 0f;
        }
        else
        {
            spawningEnemies.SetActive(true);
            PauseMenu.SetActive(false);
            Time.timeScale = 1f;
        }
    }
    private void GameResults()
    {
        
        if (gm.Player1Wins)
        {
            MusicSource2.Stop();
            gameFinished = true;
            SoundSource.PlayOneShot(winSound);
            Player1WinMenu.SetActive(true);
        }
        if (gm.Player2Wins)
        {
            MusicSource2.Stop();
            gameFinished = true;
            SoundSource.PlayOneShot(winSound);
            Player2WinMenu.SetActive(true);
        }
        if (gm.Lose)
        {
            MusicSource2.Stop();
            SoundSource.PlayOneShot(loseSound);
            gameFinished = true;
            YouLostMenu.SetActive(true);
        }
        if (gm.Win)
        {
            MusicSource2.Stop();
            gameFinished = true;
            SoundSource.PlayOneShot(winSound);
            YouWonMenu.SetActive(true);
        }
        if (gm.Draw)
        {
            MusicSource2.Stop();
            gm.Draw = false;
            SoundSource.PlayOneShot(winSound);
            gameFinished = true;
            DrawMenu.SetActive(true);
        }
    }
    public void ResumeButton()
    {
        SoundSource.PlayOneShot(buttonClick);
        paused = false;
    }
    public void RestartButton()
    {
        SoundSource.PlayOneShot(buttonClick);
        SceneManager.LoadScene("UomoBomba");
    }
    public void BackButton()
    {
        SoundSource.PlayOneShot(buttonClick);
        inOptions = false;
        OptionsMenu.SetActive(false);
        PauseMenu.SetActive(true);
    }
    public void Options()
    {
        SoundSource.PlayOneShot(buttonClick);
        inOptions = true;
        PauseMenu.SetActive(false);
        OptionsMenu.SetActive(true);
    }
    public void MainMenu()
    {
        SoundSource.PlayOneShot(buttonClick);
        SceneManager.LoadScene("MainMenu");
    }
}
