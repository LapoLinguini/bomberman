using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoxBehaviour : MonoBehaviour
{
    [SerializeField] GameObject bombPowerUp;
    [SerializeField] GameObject speedPowerUp;
    [SerializeField] GameObject explRadiusPowerUp;
    int randomSpawnOrNo;
    bool spawnPowerUp;
    int randomPowerUp;
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Explosion")
        {
            if (GameManager.multiplayer)
            {
                randomSpawnOrNo = Random.Range(0, 9);
                if (randomSpawnOrNo == 0 || randomSpawnOrNo == 1)
                    spawnPowerUp = true;
                else
                    spawnPowerUp = false;
            }
            if (!GameManager.multiplayer)
            {
                randomSpawnOrNo = Random.Range(0, 7);
                if (randomSpawnOrNo == 0 || randomSpawnOrNo == 1)
                    spawnPowerUp = true;
                else
                    spawnPowerUp = false;
            }
            if (spawnPowerUp)
            {
                randomPowerUp = Random.Range(0, 3);

                if (randomPowerUp == 0)
                    Instantiate(bombPowerUp, transform.position, Quaternion.identity);
                else if (randomPowerUp == 1)
                    Instantiate(speedPowerUp, transform.position, Quaternion.identity);
                else if (randomPowerUp == 2)
                    Instantiate(explRadiusPowerUp, transform.position, Quaternion.identity);
            }
            gameObject.SetActive(false);
        }
    }
}
