using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BombPowerUp : MonoBehaviour
{
    GameManager gm;
    private void Awake()
    {
        gm = FindObjectOfType<GameManager>();
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "Player1")
        {
            gm.TotalBombs1++;
            gm.bombCheck1= true;
            gameObject.SetActive(false);
        }
        if (collision.tag == "Player2")
        {
            gm.TotalBombs2++;
            gm.bombCheck1 = true;
            gameObject.SetActive(false);
        }

    }
}
