using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpeedPowerUp : MonoBehaviour
{
    GameManager gm;
    private void Awake()
    {
        gm = FindObjectOfType<GameManager>();
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player1")
        {
            gm.speed1 += 0.25f;
            gameObject.SetActive(false);
        }
        if (collision.tag == "Player2")
        {
            gm.speed2 += 0.25f;
            gameObject.SetActive(false);
        }

    }
}
