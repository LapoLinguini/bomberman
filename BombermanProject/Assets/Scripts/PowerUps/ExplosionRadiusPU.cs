using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplosionRadiusPU : MonoBehaviour
{
    GameManager gm;
    private void Awake()
    {
        gm = FindObjectOfType<GameManager>();
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player1")
        {
            gm.explosionRadius1++;
            gameObject.SetActive(false);
        }
        if (collision.tag == "Player2")
        {
            gm.explosionRadius2++;
            gameObject.SetActive(false);
        }

    }
}
