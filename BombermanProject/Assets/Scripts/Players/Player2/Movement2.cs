using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class Movement2 : MonoBehaviour
{
    [SerializeField] GameObject bomb;
    [SerializeField] LayerMask nonWalkLayer;

    private AudioSource SoundSource;
    public AudioClip placedBomb;

    Vector2 oldPos, newPos;

    bool canMove = true;
    bool isWall = false;
    bool canBomb = true;

    GameManager gm;


    private void Awake()
    {
        gm = FindObjectOfType<GameManager>();
        SoundSource = GetComponent<AudioSource>();
    }
    private void Start()
    {
        newPos = new Vector2(transform.position.x, transform.position.y);
    }
    private void Update()
    {
        oldPos = new Vector2(transform.position.x, transform.position.y);
        if (gm.CanMove)
        {
            MovementActionsKeys();
            WallCheck();
            FinalMovement();
        }

        if (gm.Draw)
        {
            gameObject.SetActive(false);
        }
        if (gm.Player2Wins)
        {
            gameObject.GetComponent<Collider2D>().enabled = false;
            canMove = false;
        }

    }
    private void MovementActionsKeys()
    {
        if (Input.GetKey(KeyCode.UpArrow) && canMove)
        {
            newPos = (oldPos + Vector2.up);
            WallCheck();
            canMove = false;
        }
        if (Input.GetKey(KeyCode.DownArrow) && canMove)
        {
            newPos = (oldPos + Vector2.down);
            WallCheck();
            canMove = false;
        }
        if (Input.GetKey(KeyCode.LeftArrow) && canMove)
        {
            newPos = (oldPos + Vector2.left);
            WallCheck();
            canMove = false;
        }
        if (Input.GetKey(KeyCode.RightArrow) && canMove)
        {
            newPos = (oldPos + Vector2.right);
            WallCheck();
            canMove = false;
        }
    }
    private void WallCheck()
    {
        if (Physics2D.OverlapCircle(newPos, 0.5f, nonWalkLayer))
        {
            //Debug.Log("WALL");
            isWall = true;
            newPos = transform.position;
            canMove = true;
        }
        else
        {
            isWall = false;
        }

        if (Physics2D.OverlapCircle(transform.position, 0.5f, nonWalkLayer))
        {
            //Debug.Log("BOMB");
            canBomb = false;
        }
        else
        {
            canBomb = true;
        }

        //mapB.newPos = newPos;

        //if(newPos.x == -9 || newPos.x == 9 || newPos.y == -5 || newPos.y == 5)
        //{
        //    Debug.Log("WALL");
        //    isWall = true;
        //    newPos = transform.position;
        //    canMove = true;
        //}
        //else
        //{
        //    isWall = false;
        //}
    }
    private void FinalMovement()
    {
        if (!canMove && !isWall)
        {
            //Debug.Log("moving");
            transform.position = Vector3.MoveTowards(transform.position, newPos, gm.speed2 * Time.deltaTime);
        }
        if (new Vector2(transform.position.x, transform.position.y) == newPos)
        {
            //Debug.Log("transform.position == newPos");
            canMove = true;
        }
        if (new Vector2(transform.position.x, transform.position.y) == newPos && Input.GetKey(KeyCode.Space) && gm.TotalBombs2 > 0 && canBomb)
        {
            //Debug.Log("transform.position == newPos + instantiate");
            Instantiate(bomb, transform.position, Quaternion.identity);
            SoundSource.PlayOneShot(placedBomb);
            gm.TotalBombs2--;
        }
    }
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Explosion")
        {
            gm.Player1Wins = true;
            gm.PlayerDed2 = true;
            gameObject.SetActive(false);
        }
        if (other.tag == "Player2")
        {
            gm.Draw = true;
            gm.PlayerDed1 = true;
            gameObject.SetActive(false);
        }
    }
    //private void OnDrawGizmos()
    //{
    //    Handles.color = Color.red;
    //    Handles.DrawWireDisc(newPos, Vector3.back, 0.5f);
    //}
}
