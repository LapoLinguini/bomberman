using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class Bomb2 : MonoBehaviour
{
    [Header("BombTimers: ")]
    [SerializeField] float explosionTime;
    [SerializeField] float explosionVanish;
    [Header("Explosion + Layer: ")]
    [SerializeField] GameObject explosion;
    [SerializeField] LayerMask nonWalkLayer;

    SpriteRenderer[] spriteRenderer1;

    GameManager gm;

    int numberToArray = 0;
    int numberToReplace = 1;

    bool upStop;
    bool leftStop;
    bool downStop;
    bool rightStop;

    private void Awake()
    {
        gm = FindObjectOfType<GameManager>();
        spriteRenderer1 = gameObject.GetComponentsInChildren<SpriteRenderer>();
    }

    private void Start()
    {
        StartCoroutine(ExplosionTime(explosionTime));
        IEnumerator ExplosionTime(float time)
        {
            yield return new WaitForSeconds(time);

            //al momento dell'esplosione aggiunge una bomba al player
            gm.TotalBombs2++;
            gm.boomBomb = true;

            RadiusSpawn();

            //disattiva tutti gli sprite renderer della bomba (� fatta su unity bruh)
            for (int i = 0; i < spriteRenderer1.Length; i++)
            {
                foreach (var SR in spriteRenderer1)
                {
                    SR.enabled = false;
                }
            }
            //disattiva la bomba (distruggendo cos� l'esplosione, la quale � un suo child)
            StartCoroutine(ExplosionVanish(explosionVanish));
            IEnumerator ExplosionVanish(float time)
            {
                yield return new WaitForSeconds(time);

                gameObject.SetActive(false);
            }
        }
    }

    //modifica la grandezza dell'array in modo da far variare la lunghezza del ciclo for che andr� poi a spawnare [numberToArray] numero di esplosioni
    private void RadiusSpawn()
    {
        numberToArray = 0;
        numberToReplace = 1;

        upStop = false;
        leftStop = false;
        downStop = false;
        rightStop = false;

        gm.spawnRadius = new int[gm.explosionRadius2];

        for (int i = 0; i < gm.spawnRadius.Length; i++)
        {
            gm.spawnRadius[numberToArray] = numberToReplace;

            UnBottoDiIfPerch�SonoUnCiola();

            numberToArray++;
        }

    }

    //spawna le esplosioni in base al "raggio" (gm.esplosionRadius) come child della bomba (cos� non mi esplode la hierarchy) 
    //controlla poi che non ci siano blocchi/casse che possano stoppare l'esplosione, in caso ci fossero, ferma lo spawn di ulteriori esplosioni su quell'asse
    private void UnBottoDiIfPerch�SonoUnCiola()
    {
        if (!upStop)
        {
            var Explosion1 = Instantiate(explosion, new Vector2(transform.position.x, (transform.position.y + numberToArray)), Quaternion.identity);
            Explosion1.transform.parent = gameObject.transform;
        }
        if (Physics2D.OverlapCircle(new Vector2(transform.position.x, (transform.position.y + numberToArray + 1)), 0.5f, nonWalkLayer))
        {
            upStop = true;
        }
        if (!leftStop)
        {
            var Explosion2 = Instantiate(explosion, new Vector2((transform.position.x - numberToArray), transform.position.y), Quaternion.Euler(0, 0, 90));
            Explosion2.transform.parent = gameObject.transform;
        }
        if (Physics2D.OverlapCircle(new Vector2((transform.position.x - numberToArray - 1), transform.position.y), 0.5f, nonWalkLayer))
        {
            leftStop = true;
        }
        if (!downStop)
        {
            var Explosion3 = Instantiate(explosion, new Vector2(transform.position.x, (transform.position.y - numberToArray)), Quaternion.Euler(0, 0, 180));
            Explosion3.transform.parent = gameObject.transform;
        }
        if (Physics2D.OverlapCircle(new Vector2(transform.position.x, (transform.position.y - numberToArray - 1)), 0.5f, nonWalkLayer))
        {
            downStop = true;
        }
        if (!rightStop)
        {
            var Explosion4 = Instantiate(explosion, new Vector2((transform.position.x + numberToArray), transform.position.y), Quaternion.Euler(0, 0, 270));
            Explosion4.transform.parent = gameObject.transform;
        }
        if (Physics2D.OverlapCircle(new Vector2((transform.position.x + numberToArray + 1), transform.position.y), 0.5f, nonWalkLayer))
        {
            rightStop = true;
        }
    }
}
